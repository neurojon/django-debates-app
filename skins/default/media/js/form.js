$().ready(function(){
    $("#nav_issues").attr('className',"on");
    var answer_sort_tab = "{{ tab_id }}";

    if (answer_sort_tab) {
        $("#" + answer_sort_tab).attr('className',"on");
    }

    $('#editor').TextAreaResizer();

    //toggle preview of editor
    var display = true;
    var txt = "[{% trans "hide preview" %}]";
    $('#pre-collapse').text(txt);
    $('#pre-collapse').bind('click', function(){
        txt = display ? "[{% trans "show preview" %}]" : "[{% trans "hide preview" %}]";
        display = !display;
        $('#previewer').toggle();
        $('#pre-collapse').text(txt);
    });
});

function submitClicked(e, f) {
    if(!(browserTester('chrome') || browserTester('safari'))) {
        $("input.submit")[0].disabled=true;
    }
    window.removeEventListener('beforeunload', beforeUnload, true);
    if (f) {
        f.submit();
    }
}

function beforeUnload(e) {

    if($("textarea#editor")[0].value != "") {
        return yourWorkWillBeLost(e);
    }

    var commentBoxes = $("textarea.commentBox");
    for(var index = 0; index < commentBoxes.length; index++) {
        if(commentBoxes[index].value != "") {
            return yourWorkWillBeLost(e);
        }
    }
}
window.addEventListener('beforeunload', beforeUnload, true);

