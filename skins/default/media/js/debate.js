var timeout = 500;
var closetimer = null;
var curr_displayed = null;

var argcomp_sel = null;
function argcomp_select(id)
{
	if( argcomp_sel ) argcomp_sel.style.background = 'inherit';
	argcomp_sel = document.getElementById(id);
	//argcomp_sel.style.backgroundColor = 'yellow';
	argcomp_sel.style.textDecoration = 'underline';
}
function argcomp_unselect(id)
{
	el = document.getElementById(id);
	el.style.background = 'inherit';
	el.style.textDecoration = 'none';
}

function display_opts(id)
{
	clear_close_timer();
	close_opts();

	curr_displayed = document.getElementById(id);
	curr_displayed.style.visibility = 'visible';
}

function close_opts()
{
	if( curr_displayed ) curr_displayed.style.visibility = 'hidden';
	curr_displayed = null;
}
function set_close_opts_timer()
{
	closetimer = window.setTimeout(close_opts, timeout);
}
function clear_close_timer()
{
	if(closetimer) {
		window.clearTimeout(closetimer);
		closetimer = null;
	}
}

document.onclick = close_opts;
