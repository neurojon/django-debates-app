function toggleDisplay(id) {
	div = document.getElementById(id);
	if( div ) {
		if( div.style.display != 'block') {
			div.style.display = 'block';
		} else {
			div.style.display = 'none';
		}
	}
}
