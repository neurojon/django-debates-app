# -*- coding: utf-8 -*-

try:
    from django.contrib.syndication.views import Feed, FeedDoesNotExist, add_domain
    old_version = False
except:
    from django.contrib.syndication.feeds import Feed, FeedDoesNotExist, add_domain
    old_version = True

from django.http import HttpResponse
from django.utils.encoding import smart_unicode
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe
from models import Argument
from forum import settings
from forum.modules import decorate
from forum.utils.pagination import generate_uri

@decorate(add_domain, needs_origin=False)
def add_domain(domain, url, *args, **kwargs):
    return "%s%s" % (settings.APP_BASE_URL, url)

class BaseNodeFeed(Feed):
    if old_version:
        title_template = "feeds/rss_title.html"
        description_template = "feeds/rss_description.html"

    def __init__(self, request, title, description, url):
        self._title = u"%s" % smart_unicode(title)
        self._description = mark_safe(u"%s" % smart_unicode(description))
        self._url = url

        if old_version:
            super(BaseNodeFeed, self).__init__('', request)

    def title(self):
        return u"%s" % smart_unicode(self._title)

    def link(self):
        return self._url

    def description(self):
        return u"%s" % smart_unicode(self._description)

    def item_title(self, item):
        return u"%s" % smart_unicode(item.title)

    def item_description(self, item):
        return u"%s" % smart_unicode(item.html)

    def item_link(self, item):
        return item.leaf.get_absolute_url()

    def item_author_name(self, item):
        return u"%s" % smart_unicode(item.author.username)

    def item_author_link(self, item):
        return item.author.get_profile_url()

    def item_pubdate(self, item):
        return item.added_at

    if old_version:
        def __call__(self, request):
            feedgen = self.get_feed('')
            response = HttpResponse(mimetype=feedgen.mime_type)
            feedgen.write(response, 'utf-8')
            return response


class RssArgumentFeed(BaseNodeFeed):
    def __init__(self, request, argument_list, title, description):
        url = request.path + "?" + generate_uri(request.GET, (_('page'), _('pagesize'), _('sort')))
        super(RssArgumentFeed, self).__init__(request, title, description, url)

        self._argument_list = argument_list

    def item_categories(self, item):
        return item.tagname_list()  

    def items(self):
       return self._argument_list[:30]

class RssIssueFeed(BaseNodeFeed):
    def __init__(self, request, issue_list, title, description):
        url = request.path + "?" + generate_uri(request.GET, (_('page'), _('pagesize'), _('sort')))
        super(RssIssueFeed, self).__init__(request, title, description, url)

        self._issue_list = issue_list

    def item_categories(self, item):
        return item.tagname_list()  

    def items(self):
       return self._issue_list[:30]

class RssRebuttalFeed(BaseNodeFeed):
    if old_version:
        title_template = "feeds/rss_rebuttal_title.html"

    def __init__(self, request, argument, include_comments=False):
        super(RssRebuttalFeed, self).__init__(
            request, _("Rebuttals to: %s") % smart_unicode(argument.title),
            argument.html,
            argument.get_absolute_url()
        )
        self._argument = argument
        self._include_comments = include_comments

    def items(self):
        if self._include_comments:
            qs = self._argument.all_children
        else:
            qs = self._argument.rebuttals

        return qs.filter_state(deleted=False).order_by('-added_at')[:30]

    def item_title(self, item):
        if item.node_type == "rebuttal":
            return _("Rebuttal by %s") % smart_unicode(item.author.username)
        else:
            return _("Comment by %(cauthor)s on %(pauthor)s's %(qora)s") % dict(
                cauthor=smart_unicode(item.author.username),
                pauthor=smart_unicode(item.parent.author.username),
                qora=(item.parent.node_type == "rebuttal" and _("rebuttal") or _("argument"))
            )

class RssReasonFeed(BaseNodeFeed):
    if old_version:
        title_template = "feeds/rss_title.html"

    def __init__(self, request, issue, include_comments=False):
        super(RssReasonFeed, self).__init__(
            request, _("Pro/cons to: %s") % smart_unicode(issue.title),
            issue.html,
            issue.get_absolute_url()
        )
        self._issue = issue
        self._include_comments = include_comments

    def items(self):
        if self._include_comments:
            qs = self._issue.all_children
        else:
            qs = self._issue.reasons

        return qs.filter_state(deleted=False).order_by('-added_at')[:30]

    def item_title(self, item):
        if item.node_type == "reason":
            return _("Pro/Cons by %s") % smart_unicode(item.author.username)
        else:
            return _("Comment by %(cauthor)s on %(pauthor)s's %(issue)s") % dict(
                cauthor=smart_unicode(item.author.username),
                pauthor=smart_unicode(item.parent.author.username),
                issue=(item.parent.node_type == "reason" and _("reason") or _("issue"))
            )
