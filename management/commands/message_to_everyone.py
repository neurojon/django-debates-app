from django.core.management.base import NoArgsCommand
from forum.models import User
import sys

from offline_messages.utils import create_offline_message, constants

class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        msg = None
        msg = "Hello %s, this was a test."
        if msg == None:
            print 'to run this command, please first edit the file %s' % __file__
            sys.exit(1)
        for u in User.objects.all():
            create_offline_message( u, message=msg % u.username, level=constants.INFO ) 
