from forum.models import User, SubscriptionSettings, ArgumentSubscription
from django.core.management.base import NoArgsCommand

class Command(NoArgsCommand):
    def handle_noargs(self, **options):
        users = User.objects.all()
        for u in users:
            s = SubscriptionSettings(user=u)
            s.save()

            user_arguments = u.arguments.all()

            for q in user_arguments:
                sub = ArgumentSubscription(user=u, argument=q)
                sub.save()
