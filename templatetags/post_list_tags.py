from django import template
from django.utils.translation import ugettext as _
from django.utils.safestring import mark_safe
from forum.models import Tag, MarkedTag
from forum.templatetags import argument_parser
from forum import settings

register = template.Library()

class PostItemNode(template.Node):
    template = template.loader.get_template('post_list/item.html')

    def __init__(self, post, options):
        self.post = template.Variable(post)
        self.options = options

    def render(self, context):
        return self.template.render(template.Context({
            'post': self.post.resolve(context),
            'favorite_count': self.options.get('favorite_count', 'no') == 'yes',
            'signature_type': self.options.get('signature_type', 'lite'),
        }))

class SubscriptionItemNode(template.Node):
    template = template.loader.get_template('post_list/subscription_item.html')

    def __init__(self, subscription, post, options):
        self.post = template.Variable(post)
        self.subscription = template.Variable(subscription)
        self.options = options

    def render(self, context):
        return self.template.render(template.Context({
            'post': self.post.resolve(context),
            'subscription': self.subscription.resolve(context),
            'signature_type': self.options.get('signature_type', 'lite'),
        }))

@register.tag
def post_list_item(parser, token):
    tokens = token.split_contents()[1:]
    return PostItemNode(tokens[0], argument_parser(tokens[1:]))

@register.tag
def subscription_list_item(parser, token):
    tokens = token.split_contents()[1:]
    return SubscriptionItemNode(tokens[0], tokens[1], argument_parser(tokens[2:]))

@register.inclusion_tag('post_list/sort_tabs.html')
def post_sort_tabs(sort_context):
    return sort_context

@register.inclusion_tag('post_list/related_tags.html')
def post_list_related_tags(posts):
    if len(posts):
        tags = Tag.objects.filter(nodes__id__in=[p.id for p in posts]).distinct()

        if settings.LIMIT_RELATED_TAGS:
            tags = tags[:settings.LIMIT_RELATED_TAGS]

        return {'tags': tags}
    else:
        return {'tags': False}

@register.inclusion_tag('post_list/tag_selector.html', takes_context=True)
def tag_selector(context):
    request = context['request']
    show_interesting_tags = settings.SHOW_INTERESTING_TAGS_BOX

    if request.user.is_authenticated():
        pt = MarkedTag.objects.filter(user=request.user)
        return {
            'request' : request,
            "interesting_tag_names": pt.filter(reason='good').values_list('tag__name', flat=True),
            'ignored_tag_names': pt.filter(reason='bad').values_list('tag__name', flat=True),
            'user_authenticated': True,
            'show_interesting_tags' : show_interesting_tags,
        }
    else:
        return { 'request' : request, 'user_authenticated': False, 'show_interesting_tags' : show_interesting_tags }
