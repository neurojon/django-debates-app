import os
import re
import datetime
import logging
from forum.models import User, Argument, Comment, ArgumentSubscription, SubscriptionSettings, Rebuttal
from forum.utils.mail import send_template_email
from django.utils.translation import ugettext as _
from forum.actions import CreateArgumentAction, CreateRebuttalAction, CommentAction, UserJoinsAction, PostViewAction
from forum import settings
from django.db.models import Q, F

def create_subscription_if_not_exists(argument, user):
    try:
        subscription = ArgumentSubscription.objects.get(argument=argument, user=user)
    except:
        subscription = ArgumentSubscription(argument=argument, user=user)
        subscription.save()

def filter_subscribers(subscribers):
    subscribers = subscribers.exclude(is_active=False)

    if settings.DONT_NOTIFY_UNVALIDATED:
        return subscribers.exclude(email_isvalid=False)
    else:
        return subscribers

def argument_posted(action, new):
    argument = action.node

    subscribers = User.objects.filter(
            Q(subscription_settings__enable_notifications=True, subscription_settings__new_argument='i') |
            (Q(subscription_settings__new_argument_watched_tags='i') &
              Q(marked_tags__name__in=argument.tagnames.split(' ')) &
              Q(tag_selections__reason='good'))
    ).exclude(id=argument.author.id).distinct()

    subscribers = filter_subscribers(subscribers)

    send_template_email(subscribers, "notifications/newargument.html", {'argument': argument})

    subscription = ArgumentSubscription(argument=argument, user=argument.author)
    subscription.save()

    new_subscribers = User.objects.filter(
            Q(subscription_settings__all_arguments=True) |
            Q(subscription_settings__all_arguments_watched_tags=True,
                    marked_tags__name__in=argument.tagnames.split(' '),
                    tag_selections__reason='good'))

    for user in new_subscribers:
        create_subscription_if_not_exists(argument, user)

CreateArgumentAction.hook(argument_posted)


def rebuttal_posted(action, new):
    rebuttal = action.node
    argument = rebuttal.argument

    subscribers = argument.subscribers.filter(
            subscription_settings__enable_notifications=True,
            subscription_settings__notify_rebuttals=True,
            subscription_settings__subscribed_arguments='i'
    ).exclude(id=rebuttal.author.id).distinct()

    subscribers = filter_subscribers(subscribers)

    send_template_email(subscribers, "notifications/newrebuttal.html", {'rebuttal': rebuttal})

    create_subscription_if_not_exists(argument, rebuttal.author)

CreateRebuttalAction.hook(rebuttal_posted)


def comment_posted(action, new):
    comment = action.node
    post = comment.parent

    if post.__class__ == Argument:
        argument = post
    else:
        argument = post.argument

    q_filter = Q(subscription_settings__notify_comments=True) | Q(subscription_settings__notify_comments_own_post=True, id=post.author.id)

    inreply = re.search('@\w+', comment.comment)
    if inreply is not None:
        q_filter = q_filter | Q(subscription_settings__notify_reply_to_comments=True,
                                username__istartswith=inreply.group(0)[1:],
                                nodes__parent=post, nodes__node_type="comment")

    subscribers = argument.subscribers.filter(
            q_filter, subscription_settings__subscribed_arguments='i', subscription_settings__enable_notifications=True
    ).exclude(id=comment.user.id).distinct()

    subscribers = filter_subscribers(subscribers)


    send_template_email(subscribers, "notifications/newcomment.html", {'comment': comment})

    create_subscription_if_not_exists(argument, comment.user)

CommentAction.hook(comment_posted)


def member_joined(action, new):
    subscribers = User.objects.filter(
            subscription_settings__enable_notifications=True,
            subscription_settings__member_joins='i'
    ).exclude(id=action.user.id).distinct()

    subscribers = filter_subscribers(subscribers)

    send_template_email(subscribers, "notifications/newmember.html", {'newmember': action.user})

UserJoinsAction.hook(member_joined)

def argument_viewed(action, new):
    if not action.viewuser.is_authenticated():
        return

    try:
        subscription = ArgumentSubscription.objects.get(argument=action.node, user=action.viewuser)
        subscription.last_view = datetime.datetime.now()
        subscription.save()
    except:
        if action.viewuser.subscription_settings.arguments_viewed:
            subscription = ArgumentSubscription(argument=action.node, user=action.viewuser)
            subscription.save()

PostViewAction.hook(argument_viewed)

def issue_viewed(action, new):
    if not action.viewuser.is_authenticated():
        return

    try:
        subscription = IssueSubscription.objects.get(issue=action.node, user=action.viewuser)
        subscription.last_view = datetime.datetime.now()
        subscription.save()
    except:
        if action.viewuser.subscription_settings.issues_viewed:
            subscription = IssueSubscription(issue=action.node, user=action.viewuser)
            subscription.save()

PostViewAction.hook(issue_viewed)

#todo: translate this
#record_rebuttal_event_re = re.compile("You have received (a|\d+) .*new response.*")
#def record_rebuttal_event(instance, created, **kwargs):
#    if created:
#        q_author = instance.argument.author
#        found_match = False
#        #print 'going through %d messages' % q_author.message_set.all().count()
#        for m in q_author.message_set.all():
##            #print m.message
# #           match = record_rebuttal_event_re.search(m.message)
#            if match:
#                found_match = True
#                try:
#                    cnt = int(match.group(1))
#                except:
#                    cnt = 1
##                m.message = u"You have received %d <a href=\"%s?sort=responses\">new responses</a>."\
# #                           % (cnt+1, q_author.get_profile_url())
#
#                m.save()
#                break
#        if not found_match:
#            msg = u"You have received a <a href=\"%s?sort=responses\">new response</a>."\
#                    % q_author.get_profile_url()
#
#            q_author.message_set.create(message=msg)
#
#post_save.connect(record_rebuttal_event, sender=Rebuttal)
