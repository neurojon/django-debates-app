from base import *
from node import NodeQuerySet
import re
from tag import Tag
from django.utils.translation import ugettext as _

class RootNodeManager(NodeManager):
    def get_query_set(self):
        qs = NodeQuerySet(self.model)

        # RootNode.objects should include all non-rootnode objects, (ie. not a comment)
        if self.model is RootNode:
            qs = qs.exclude(node_type='comment')
        elif self.model is not Node:
            qs = qs.filter(node_type=self.model.get_type())

        return qs

    def search(self, keywords):
        return False, self.filter(models.Q(title__icontains=keywords) | models.Q(body__icontains=keywords))

class RootNode(Node):
    """
    This is an "abstract" class for the primary nodes, such as Question, 
    Issue, or Argument, for which can take "replies" such as Answer,
    ProCon, or Rebuttal.
    """
    class Meta(Node.Meta):
        proxy = True

    #answer_count = DenormalizedField("children", ~models.Q(state_string__contains="(deleted)"), node_type="answer")
    #accepted_count = DenormalizedField("children", ~models.Q(state_string__contains="(deleted)"), node_type="answer", marked=True)
    favorite_count = DenormalizedField("actions", action_type="favorite", canceled=False)

    objects = RootNodeManager()

    @property
    def closed(self):
        return self.nis.closed

    @property    
    def view_count(self):
        return self.extra_count

    @property
    def headline(self):
        headline = self.title
        if not headline:
            headline = self.body
            headline = re.sub('<[^>]+>',' ',headline)
            if len(headline) > 43:
                headline = headline[0:40]+'...'
        return self._decorate_headline(headline)

    def _decorate_headline(self, headline):
        if self.nis.deleted:
            headline =  _('[deleted] ') + headline

        if self.nis.closed:
            headline = _('[closed] ') + headline

        if not self.nis.published:
            headline = _('[draft] ') + headline

        return headline

    @models.permalink    
    def get_absolute_url(self):
        return ('issue', (), {'id': self.id, 'slug': django_urlquote(slugify(self.title))})
        
    def meta_description(self):
        pass

    def get_revision_url(self):
        pass

    def get_related(self, count=10):
        cache_key = '%s.related:%d:%d' % (settings.APP_URL, count, self.id)
        related_list = cache.get(cache_key)

        if related_list is None:
            related_list = RootNode.objects.filter_state(deleted=False,published=True).values('id').filter(tags__id__in=[t.id for t in self.tags.all()]
            ).exclude(id=self.id).annotate(frequency=models.Count('id')).order_by('-frequency')[:count]
            cache.set(cache_key, related_list, 60 * 60)

        return [RootNode.objects.get(id=r['id']) for r in related_list]



class RootNodeSubscription(models.Model):
    user = models.ForeignKey(User)
    issue = models.ForeignKey(Node)
    auto_subscription = models.BooleanField(default=True)
    last_view = models.DateTimeField(default=datetime.datetime.now())

    class Meta:
        app_label = 'forum'


class RootNodeRevision(NodeRevision):
    class Meta:
        proxy = True

