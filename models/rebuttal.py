from base import *
from django.utils.translation import ugettext as _

class Rebuttal(Node):
    friendly_name = _("rebuttal")

    class Meta(Node.Meta):
        proxy = True

#    @property
#    def accepted(self):
#        return self.nis.accepted

    @property
    def headline(self):
        return self.argument.headline

    def get_absolute_url(self):
        return '%s/%s' % (self.argument.get_absolute_url(), self.id)


class RebuttalRevision(NodeRevision):
    class Meta:
        proxy = True
