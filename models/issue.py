from base import *
from tag import Tag
from rootnode import *
from django.utils.translation import ugettext as _
from position import Position

class Issue(RootNode):
    """ It is assumed that there are two positions for each issue. """
    class Meta(Node.Meta):
        proxy = True

    reply_count = DenormalizedField("positions", ~models.Q(state_string__contains="(deleted)"))
    favorite_count = DenormalizedField("actions", action_type="favorite", canceled=False)

    friendly_name = _("issue")

    @property
    def positions(self):
        return Position.objects.filter(parent=self,node_type="position");

#    @property
#    def reasons(self):
#        return Argument.objects.filter(parent__parent=self);
        #return self.children.children.filter(node_type="reason")

    @models.permalink    
    def get_absolute_url(self):
        return ('issue', (), {'id': self.id, 'slug': django_urlquote(slugify(self.title))})
        
    def get_revision_url(self):
        return reverse('issue_revisions', args=[self.id])

    def get_related_issues(self, count=10):
        cache_key = '%s.related_issues:%d:%d' % (settings.APP_URL, count, self.id)
        related_list = cache.get(cache_key)

        if related_list is None:
            related_list = Issue.objects.filter_state(deleted=False,published=True).values('id').filter(tags__id__in=[t.id for t in self.tags.all()]
            ).exclude(id=self.id).annotate(frequency=models.Count('id')).order_by('-frequency')[:count]
            cache.set(cache_key, related_list, 60 * 60)

        return [Issue.objects.get(id=r['id']) for r in related_list]

    

"""
    Probably should replace this with RootNodeSubscription?
"""
class IssueSubscription(models.Model):
    user = models.ForeignKey(User)
    issue = models.ForeignKey(Node)
    auto_subscription = models.BooleanField(default=True)
    last_view = models.DateTimeField(default=datetime.datetime.now())

    class Meta:
        app_label = 'forum'


class IssueRevision(RootNodeRevision):
    class Meta:
        proxy = True
