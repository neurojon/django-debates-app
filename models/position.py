from base import *
from django.utils.translation import ugettext as _
from rootnode import RootNode, RootNodeRevision

class Position(RootNode):
    friendly_name = _("position")

    class Meta(Node.Meta):
        proxy = True

    reply_count = DenormalizedField("reasons", ~models.Q(state_string__contains="(deleted)"))

    @property 
    def friendly_count_type_plural(self):
        return _("arguments")

    @property 
    def friendly_count_type_singular(self):
        return _("argument")

    @property
    def reasons(self):
        return self.children.filter(node_type="argument")

    @property
    def accepted(self):
        return self.nis.accepted

    def get_absolute_url(self):
        if not self.issue is None:
            return '%s/%s' % (self.issue.get_absolute_url(), self.id)
        else:
            return reverse('position', kwargs={'id': self.id, 'slug': django_urlquote(slugify(self.title))})


class PositionRevision(RootNodeRevision):
    class Meta:
        proxy = True
