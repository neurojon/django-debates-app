from django.utils.html import strip_tags
from django.utils.translation import ugettext as _
from forum.models.action import ActionProxy
from forum.models import Comment, Issue, Position, Argument, Rebuttal, NodeRevision
import logging

from offline_messages.utils import create_offline_message, constants

class NodeEditAction(ActionProxy):
    def create_revision_data(self, initial=False, **data):
        revision_data = dict(summary=data.get('summary', (initial and _('Initial revision') or '')), body=data['text'])

        if data.get('title', None):
            revision_data['title'] = strip_tags(data['title'].strip())

        if data.get('tags', None):
            revision_data['tagnames'] = data['tags'].strip()

        return revision_data

class CreateArgumentAction(NodeEditAction):
    verb = _("argued")

    def process_data(self, **data):
        processed_data = self.create_revision_data(True, **data)
        if 'added_at' in data:
            processed_data['added_at'] = data['added_at']
#        if 'parent' in data:
#            processed_data['parent'] = data['parent']

        argument = Argument(author=self.user, **processed_data)
        argument.save()

        self.node = argument

        create_offline_message(self.user, message=self.describe(self.user), level=constants.INFO )

    def describe(self, viewer=None):
        return _("%(user)s argued %(argument)s") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'argument': self.hyperlink(self.node.get_absolute_url(), self.node.title)
        }

class CreateIssueAction(NodeEditAction):
    verb = _("presented issue for debate")

    def process_data(self, **data):
        processed_data = self.create_revision_data(True, **data)
        if 'added_at' in data:
            processed_data['added_at'] = data['added_at']

        issue = Issue(author=self.user, **processed_data)
        issue.save()

        self.node = issue

        create_offline_message(self.user, message=self.describe(self.user), level=constants.INFO )

    def describe(self, viewer=None):
        return _("%(user)s created the issue %(issue)s") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'issue': self.hyperlink(self.node.get_absolute_url(), self.node.title)
        }

class CreatePositionAction(NodeEditAction):
    verb = _("added position to issue")

    def process_data(self, **data):
        self.node = Position(author=self.user, parent=data['issue'], **self.create_revision_data(True, **data))
        self.node.save()

    def process_action(self):
        if not self.node.issue is None:
            self.node.issue.reset_reply_count_cache()
        create_offline_message(self.user, message=self.describe(self.user), level=constants.INFO )

    def describe(self, viewer=None):
        position = self.node
        issue = self.node.parent
        if not issue is None:
	        return _("%(user)s created the position %(position)s for %(issue)s") % {
	            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
	            'position': self.hyperlink(position.get_absolute_url(), position.title),
	            'issue': self.hyperlink(issue.get_absolute_url(), issue.title),
	        }
        else:
	        return _("%(user)s created the position %(position)s") % {
	            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
	            'position': self.hyperlink(position.get_absolute_url(), position.title),
	        }

class CreateRebuttalAction(NodeEditAction):
    verb = _("rebutted")

    def process_data(self, **data):
        rebuttal = Rebuttal(author=self.user, parent=data['argument'], **self.create_revision_data(True, **data))
        rebuttal.save()
        self.node = rebuttal

    def process_action(self):
        self.node.argument.reset_reply_count_cache()

        create_offline_message(self.user, message=self.describe(self.user), level=constants.INFO )


    def describe(self, viewer=None):
        argument = self.node.parent
        return _("%(user)s rebutted %(asker)s on %(argument)s") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'asker': self.hyperlink(argument.author.get_profile_url(), self.friendly_username(viewer, argument.author)),
            'argument': self.hyperlink(self.node.get_absolute_url(), argument.title)
        }


class CreateReasonAction(NodeEditAction):
    verb = _("gave reason against position")
    action_description = _("%(user)s opposed %(position)s")

    def __init__(self, opposes_parent, *args, **kwargs):
        super(CreateReasonAction,self).__init__(*args,**kwargs)
        if opposes_parent:
            self.verb = _("gave reason against position")
            self.action_description = _("%(user)s opposed %(position)s")
        else:
            self.verb = _("gave support for position")
            self.action_description = _("%(user)s supported %(position)s")

    def process_data(self, **data):
        reason = Argument(author=self.user, parent=data['position'], 
            opposes_parent=data['opposes_parent'], **self.create_revision_data(True, **data))
        reason.save()
        self.node = reason

    def process_action(self):
        self.node.position.reset_reply_count_cache()
        create_offline_message(self.user, message=self.describe(self.user), level=constants.INFO )

    def describe(self, viewer=None):
        position = self.node.parent
        return _(self.action_description) % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'position': self.hyperlink(position.get_absolute_url(), position.title),
        }

class CommentAction(ActionProxy):
    verb = _("commented")

    def process_data(self, text='', parent=None):
        comment = Comment(author=self.user, parent=parent, body=text)
        comment.save()
        self.node = comment

    def describe(self, viewer=None):
        return _("%(user)s commented on %(post_desc)s") % {
            'user': self.hyperlink(self.node.author.get_profile_url(), self.friendly_username(viewer, self.node.author)),
            'post_desc': self.describe_node(viewer, self.node.parent)
        }

class ReviseAction(NodeEditAction):
    verb = _("edited")

    def process_data(self, **data):
        revision_data = self.create_revision_data(**data)
        revision = self.node.create_revision(self.user, **revision_data)
        self.extra = revision.revision

    def process_action(self):
        self.node.last_edited = self
        self.node.save()

    def describe(self, viewer=None):
        return _("%(user)s edited %(post_desc)s") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'post_desc': self.describe_node(viewer, self.node)
        }

    def get_absolute_url(self):
        return self.node.get_revisions_url()


class PublishAction(NodeEditAction):
    verb = _("published")

    def process_data(self, **data):
        revision_data = self.create_revision_data(**data)
        revision = self.node.create_revision(self.user, **revision_data)
        self.extra = revision.revision

    def process_action(self):
        self.node.mark_published(self)
        self.node.save()

    def cancel_action(self): # don't know if this can be used
        self.node.mark_published(None)

    def describe(self, viewer=None):
        return _("%(user)s published %(post_desc)s") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'post_desc': self.describe_node(viewer, self.node)
        }

    def get_absolute_url(self):
        return self.node.get_revisions_url()


class PromoteAction(NodeEditAction):
    verb = _("promoted")

    def process_data(self, **data):
        revision_data = self.create_revision_data(**data)
        revision = self.node.create_revision(self.user, **revision_data)
        self.extra = revision.revision

    def process_action(self):
        self.node.mark_promoted(self)
        self.node.save()

    def cancel_action(self): # don't know if this can be used
        self.node.mark_promoted(None)

    def describe(self, viewer=None):
        return _("%(user)s promoted %(post_desc)s") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'post_desc': self.describe_node(viewer, self.node)
        }

    def get_absolute_url(self):
        return self.node.get_revisions_url()


class RetagAction(ActionProxy):
    verb = _("retagged")

    def process_data(self, tagnames=''):
        active = self.node.active_revision
        revision_data = dict(summary=_('Retag'), title=active.title, tagnames=strip_tags(tagnames.strip()), body=active.body)
        revision = self.node.create_revision(self.user, **revision_data)
        self.extra = revision.revision

    def process_action(self):
        self.node.last_edited = self
        self.node.save()

    def describe(self, viewer=None):
        return _("%(user)s retagged %(post_desc)s") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'post_desc': self.describe_node(viewer, self.node)
        }

    def get_absolute_url(self):
        return self.node.get_revisions_url()

class RollbackAction(ActionProxy):
    verb = _("reverted")

    def process_data(self, activate=None):
        previous = self.node.active_revision
        self.node.activate_revision(self.user, activate)
        self.extra = "%d:%d" % (previous.revision, activate.revision)

    def process_action(self):
        self.node.last_edited = self
        self.node.save()

    def describe(self, viewer=None):
        revisions = [NodeRevision.objects.get(node=self.node, revision=int(n)) for n in self.extra.split(':')]

        return _("%(user)s reverted %(post_desc)s from revision %(initial)d (%(initial_sum)s) to revision %(final)d (%(final_sum)s)") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'post_desc': self.describe_node(viewer, self.node),
            'initial': revisions[0].revision, 'initial_sum': revisions[0].summary,
            'final': revisions[1].revision, 'final_sum': revisions[1].summary,
        }

    def get_absolute_url(self):
        return self.node.get_revisions_url()

class CloseAction(ActionProxy):
    verb = _("closed")

    def process_action(self):
        self.node.marked = True
        self.node.nstate.closed = self
        self.node.last_edited = self
        self.node.update_last_activity(self.user, save=True)

    def cancel_action(self):
        self.node.marked = False
        self.node.nstate.closed = None
        self.node.update_last_activity(self.user, save=True)

    def describe(self, viewer=None):
        return _("%(user)s closed %(post_desc)s: %(reason)s") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'post_desc': self.describe_node(viewer, self.node),
            'reason': self.extra
        }

class RebuttalToCommentAction(ActionProxy):
    verb = _("converted")

    def process_data(self, new_parent=None):
        self.node.parent = new_parent
        self.node.node_type = "comment"

        for comment in self.node.comments.all():
            comment.parent = new_parent
            comment.save()

        self.node.last_edited = self
        self.node.update_last_activity(self.user, save=True)
        try:
            self.node.abs_parent.reset_answer_count_cache()
        except AttributeError:
            pass

    def describe(self, viewer=None):
        return _("%(user)s converted an rebuttal to %(argument)s into a comment") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'argument': self.describe_node(viewer, self.node.abs_parent),
        }

class CommentToRebuttalAction(ActionProxy):
    verb = _("converted")

    def process_data(self, argument):
        self.node.parent = argument
        self.node.node_type = "rebuttal"
        self.node.last_edited = self
        self.node.update_last_activity(self.user, save=True)

        # Now updated the cached data
        argument.reset_answer_count_cache()

    def describe(self, viewer=None):
        return _("%(user)s converted comment on %(argument)s into a rebuttal") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'argument': self.describe_node(viewer, self.node.abs_parent),
        }
class CommentToArgumentAction(NodeEditAction):
    verb = _("converted")

    def process_data(self, **data):
        revision_data = self.create_revision_data(**data)
        revision = self.node.create_revision(self.user, **revision_data)

        self.extra = {
            'covert_revision': revision.revision,
        }

        self.node.node_type = "argument"
        self.node.parent = None
        self.node.abs_parent = None

    def process_action(self):
        self.node.last_edited = self
        self.node.save()

    def describe(self, viewer=None):
        return _("%(user)s converted comment on %(argument)s to a new argument") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'argument': self.describe_node(viewer, self.node.abs_parent),
        }

class RebuttalToArgumentAction(NodeEditAction):
    verb = _("converted to argument")

    def process_data(self,  **data):
        revision_data = self.create_revision_data(**data)
        revision = self.node.create_revision(self.user, **revision_data)

        original_argument = self.node.argument

        self.extra = {
            'covert_revision': revision.revision,
            'original_argument': original_argument
        }

        self.node.node_type = "argument"
        self.node.parent = None
        self.node.abs_parent = None

        original_argument.reset_answer_count_cache()

    def process_action(self):
        self.node.last_edited = self
        self.node.save()


    def describe(self, viewer=None):
        return _("%(user)s converted an argument to %(argument)s into a separate argument") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'argument': self.describe_node(viewer, self.node.abs_parent),
        }

class WikifyAction(ActionProxy):
    verb = _("wikified")

    def process_action(self):
        self.node.nstate.wiki = self
        self.node.last_edited = self
        self.node.update_last_activity(self.user, save=True)

    def cancel_action(self):
        self.node.nstate.wiki = None
        self.node.update_last_activity(self.user, save=True)

    def describe(self, viewer=None):
        return _("%(user)s marked %(node)s as community wiki.") % {
            'user': self.hyperlink(self.user.get_profile_url(), self.friendly_username(viewer, self.user)),
            'node': self.describe_node(viewer, self.node),
        }

