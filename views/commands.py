# -*- coding: utf-8 -*-

import datetime
import logging

from urllib import urlencode

from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.utils import simplejson
from django.utils.encoding import smart_unicode
from django.utils.translation import ungettext, ugettext as _
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404, render_to_response

from forum.models import *
from forum.utils.decorators import ajax_login_required
from forum.actions import *
from forum.modules import decorate
from forum import settings

from decorators import command, CommandException, RefreshPageCommand

class NotEnoughRepPointsException(CommandException):
    def __init__(self, action, user_reputation=None, reputation_required=None, node=None):
        if reputation_required is not None and user_reputation is not None:
            message = _(
                """Sorry, but you don't have enough reputation points to %(action)s.<br />
                The minimum reputation required is %(reputation_required)d (yours is %(user_reputation)d).
                Please check the <a href='%(faq_url)s'>FAQ</a>"""
            ) % {
                'action': action,
                'faq_url': reverse('faq'),
                'reputation_required' : reputation_required,
                'user_reputation' : user_reputation,
            }
        else:
            message = _(
                """Sorry, but you don't have enough reputation points to %(action)s.<br />Please check the <a href='%(faq_url)s'>faq</a>"""
            ) % {'action': action, 'faq_url': reverse('faq')}
        super(NotEnoughRepPointsException, self).__init__(message)

class CannotDoOnOwnException(CommandException):
    def __init__(self, action):
        super(CannotDoOnOwnException, self).__init__(
                _(
                        """Sorry but you cannot %(action)s your own post.<br />Please check the <a href='%(faq_url)s'>faq</a>"""
                        ) % {'action': action, 'faq_url': reverse('faq')}
                )

class AnonymousNotAllowedException(CommandException):
    def __init__(self, action):
        super(AnonymousNotAllowedException, self).__init__(
                _(
                        """Sorry but anonymous users cannot %(action)s.<br />Please login or create an account <a href='%(signin_url)s'>here</a>."""
                        ) % {'action': action, 'signin_url': reverse('auth_signin')}
                )

class NotEnoughLeftException(CommandException):
    def __init__(self, action, limit):
        super(NotEnoughLeftException, self).__init__(
                _(
                        """Sorry, but you don't have enough %(action)s left for today..<br />The limit is %(limit)s per day..<br />Please check the <a href='%(faq_url)s'>faq</a>"""
                        ) % {'action': action, 'limit': limit, 'faq_url': reverse('faq')}
                )

class CannotDoubleActionException(CommandException):
    def __init__(self, action):
        super(CannotDoubleActionException, self).__init__(
                _(
                        """Sorry, but you cannot %(action)s twice the same post.<br />Please check the <a href='%(faq_url)s'>faq</a>"""
                        ) % {'action': action, 'faq_url': reverse('faq')}
                )


@decorate.withfn(command)
def vote_post(request, id, vote_type):
    post = get_object_or_404(Node, id=id).leaf
    user = request.user

    if not user.is_authenticated():
        raise AnonymousNotAllowedException(_('vote'))

    if user == post.author:
        raise CannotDoOnOwnException(_('vote'))

    if not (vote_type == 'up' and user.can_vote_up() or user.can_vote_down()):
        reputation_required = int(settings.REP_TO_VOTE_UP) if vote_type == 'up' else int(settings.REP_TO_VOTE_DOWN)
        action_type = vote_type == 'up' and _('upvote') or _('downvote')
        raise NotEnoughRepPointsException(action_type, user_reputation=user.reputation, reputation_required=reputation_required, node=post)

    user_vote_count_today = user.get_vote_count_today()
    user_can_vote_count_today = user.can_vote_count_today()

    if user_vote_count_today >= user.can_vote_count_today():
        raise NotEnoughLeftException(_('votes'), str(settings.MAX_VOTES_PER_DAY))

    new_vote_cls = (vote_type == 'up') and VoteUpAction or VoteDownAction
    score_inc = 0

    old_vote = VoteAction.get_action_for(node=post, user=user)

    if old_vote:
        if old_vote.action_date < datetime.datetime.now() - datetime.timedelta(days=int(settings.DENY_UNVOTE_DAYS)):
            raise CommandException(
                    _("Sorry but you cannot cancel a vote after %(ndays)d %(tdays)s from the original vote") %
                    {'ndays': int(settings.DENY_UNVOTE_DAYS),
                     'tdays': ungettext('day', 'days', int(settings.DENY_UNVOTE_DAYS))}
                    )

        old_vote.cancel(ip=request.META['REMOTE_ADDR'])
        score_inc = (old_vote.__class__ == VoteDownAction) and 1 or -1
        vote_type = "none"
    else:
        new_vote_cls(user=user, node=post, ip=request.META['REMOTE_ADDR']).save()
        score_inc = (new_vote_cls == VoteUpAction) and 1 or -1

    response = {
    'commands': {
    'update_post_score': [id, score_inc],
    'update_user_post_vote': [id, vote_type]
    }
    }

    votes_left = (user_can_vote_count_today - user_vote_count_today) + (vote_type == 'none' and -1 or 1)

    if int(settings.START_WARN_VOTES_LEFT) >= votes_left:
        response['message'] = _("You have %(nvotes)s %(tvotes)s left today.") % \
                    {'nvotes': votes_left, 'tvotes': ungettext('vote', 'votes', votes_left)}

    return response

@decorate.withfn(command)
def flag_post(request, id):
    if not request.POST:
        return render_to_response('node/report.html', {'types': settings.FLAG_TYPES})

    post = get_object_or_404(Node, id=id)
    user = request.user

    if not user.is_authenticated():
        raise AnonymousNotAllowedException(_('flag posts'))

    if user == post.author:
        raise CannotDoOnOwnException(_('flag'))

    if not (user.can_flag_offensive(post)):
        raise NotEnoughRepPointsException(_('flag posts'))

    user_flag_count_today = user.get_flagged_items_count_today()

    if user_flag_count_today >= int(settings.MAX_FLAGS_PER_DAY):
        raise NotEnoughLeftException(_('flags'), str(settings.MAX_FLAGS_PER_DAY))

    try:
        current = FlagAction.objects.get(canceled=False, user=user, node=post)
        raise CommandException(
                _("You already flagged this post with the following reason: %(reason)s") % {'reason': current.extra})
    except ObjectDoesNotExist:
        reason = request.POST.get('prompt', '').strip()

        if not len(reason):
            raise CommandException(_("Reason is empty"))

        FlagAction(user=user, node=post, extra=reason, ip=request.META['REMOTE_ADDR']).save()

    return {'message': _("Thank you for your report. A moderator will review your submission shortly.")}

@decorate.withfn(command)
def like_comment(request, id):
    comment = get_object_or_404(Comment, id=id)
    user = request.user

    if not user.is_authenticated():
        raise AnonymousNotAllowedException(_('like comments'))

    if user == comment.user:
        raise CannotDoOnOwnException(_('like'))

    if not user.can_like_comment(comment):
        raise NotEnoughRepPointsException( _('like comments'), node=comment)

    like = VoteAction.get_action_for(node=comment, user=user)

    if like:
        like.cancel(ip=request.META['REMOTE_ADDR'])
        likes = False
    else:
        VoteUpCommentAction(node=comment, user=user, ip=request.META['REMOTE_ADDR']).save()
        likes = True

    return {
    'commands': {
    'update_post_score': [comment.id, likes and 1 or -1],
    'update_user_post_vote': [comment.id, likes and 'up' or 'none']
    }
    }

@decorate.withfn(command)
def delete_comment(request, id):
    comment = get_object_or_404(Comment, id=id)
    user = request.user

    if not user.is_authenticated():
        raise AnonymousNotAllowedException(_('delete comments'))

    if not user.can_delete_comment(comment):
        raise NotEnoughRepPointsException( _('delete comments'))

    if not comment.nis.deleted:
        DeleteAction(node=comment, user=user, ip=request.META['REMOTE_ADDR']).save()

    return {
    'commands': {
    'remove_comment': [comment.id],
    }
    }

@decorate.withfn(command)
def mark_favorite(request, id):
    node = get_object_or_404(Node, id=id)

    if not request.user.is_authenticated():
        raise AnonymousNotAllowedException(_('mark a argument as favorite'))

    try:
        favorite = FavoriteAction.objects.get(canceled=False, node=node, user=request.user)
        favorite.cancel(ip=request.META['REMOTE_ADDR'])
        added = False
    except ObjectDoesNotExist:
        FavoriteAction(node=node, user=request.user, ip=request.META['REMOTE_ADDR']).save()
        added = True

    return {
    'commands': {
    'update_favorite_count': [added and 1 or -1],
    'update_favorite_mark': [added and 'on' or 'off']
    }
    }

@decorate.withfn(command)
def comment(request, id):
    post = get_object_or_404(Node, id=id)
    user = request.user

    if not user.is_authenticated():
        raise AnonymousNotAllowedException(_('comment'))

    if not request.method == 'POST':
        raise CommandException(_("Invalid request"))

    comment_text = request.POST.get('comment', '').strip()

    if not len(comment_text):
        raise CommandException(_("Comment is empty"))

    if len(comment_text) < settings.FORM_MIN_COMMENT_BODY:
        raise CommandException(_("At least %d characters required on comment body.") % settings.FORM_MIN_COMMENT_BODY)

    if len(comment_text) > settings.FORM_MAX_COMMENT_BODY:
        raise CommandException(_("No more than %d characters on comment body.") % settings.FORM_MAX_COMMENT_BODY)

    if 'id' in request.POST:
        comment = get_object_or_404(Comment, id=request.POST['id'])

        if not user.can_edit_comment(comment):
            raise NotEnoughRepPointsException( _('edit comments'))

        comment = ReviseAction(user=user, node=comment, ip=request.META['REMOTE_ADDR']).save(
                data=dict(text=comment_text)).node
    else:
        if not user.can_comment(post):
            raise NotEnoughRepPointsException( _('comment'))

        comment = CommentAction(user=user, ip=request.META['REMOTE_ADDR']).save(
                data=dict(text=comment_text, parent=post)).node

    if comment.active_revision.revision == 1:
        return {
        'commands': {
        'insert_comment': [
                id, comment.id, comment.comment, user.decorated_name, user.get_profile_url(),
                reverse('delete_comment', kwargs={'id': comment.id}),
                reverse('node_markdown', kwargs={'id': comment.id}),
                reverse('convert_comment', kwargs={'id': comment.id}),
                user.can_convert_comment_to_rebuttal(comment),
                bool(settings.SHOW_LATEST_COMMENTS_FIRST)
                ]
        }
        }
    else:
        return {
        'commands': {
        'update_comment': [comment.id, comment.comment]
        }
        }

@decorate.withfn(command)
def node_markdown(request, id):
    user = request.user

    if not user.is_authenticated():
        raise AnonymousNotAllowedException(_('accept rebuttals'))

    node = get_object_or_404(Node, id=id)
    return HttpResponse(node.active_revision.body, mimetype="text/plain")

@decorate.withfn(command)
def delete_post(request, id):
    post = get_object_or_404(Node, id=id)
    user = request.user

    if not user.is_authenticated():
        raise AnonymousNotAllowedException(_('delete posts'))

    if not (user.can_delete_post(post)):
        raise NotEnoughRepPointsException(_('delete posts'))

    ret = {'commands': {}}

    if post.nis.deleted:
        post.nstate.deleted.cancel(user, ip=request.META['REMOTE_ADDR'])
        ret['commands']['unmark_deleted'] = [post.node_type, id]
    else:
        DeleteAction(node=post, user=user, ip=request.META['REMOTE_ADDR']).save()

        ret['commands']['mark_deleted'] = [post.node_type, id]

    return ret

@decorate.withfn(command)
def close(request, id, close):
    if close and not request.POST:
        return render_to_response('node/report.html', {'types': settings.CLOSE_TYPES})

    argument = get_object_or_404(Argument, id=id)
    user = request.user

    if not user.is_authenticated():
        raise AnonymousNotAllowedException(_('close arguments'))

    if argument.nis.closed:
        if not user.can_reopen_argument(argument):
            raise NotEnoughRepPointsException(_('reopen arguments'))

        argument.nstate.closed.cancel(user, ip=request.META['REMOTE_ADDR'])
    else:
        if not request.user.can_close_argument(argument):
            raise NotEnoughRepPointsException(_('close arguments'))

        reason = request.POST.get('prompt', '').strip()

        if not len(reason):
            raise CommandException(_("Reason is empty"))

        CloseAction(node=argument, user=user, extra=reason, ip=request.META['REMOTE_ADDR']).save()

    return RefreshPageCommand()

@decorate.withfn(command)
def wikify(request, id):
    node = get_object_or_404(Node, id=id)
    user = request.user

    if not user.is_authenticated():
        raise AnonymousNotAllowedException(_('mark posts as community wiki'))

    if node.nis.wiki:
        if not user.can_cancel_wiki(node):
            raise NotEnoughRepPointsException(_('cancel a community wiki post'))

        if node.nstate.wiki.action_type == "wikify":
            node.nstate.wiki.cancel()
        else:
            node.nstate.wiki = None
    else:
        if not user.can_wikify(node):
            raise NotEnoughRepPointsException(_('mark posts as community wiki'))

        WikifyAction(node=node, user=user, ip=request.META['REMOTE_ADDR']).save()

    return RefreshPageCommand()

@decorate.withfn(command)
def convert_to_comment(request, id):
    user = request.user
    rebuttal = get_object_or_404(Rebuttal, id=id)
    argument = rebuttal.argument

    # Check whether the user has the required permissions
    if not user.is_authenticated():
        raise AnonymousNotAllowedException(_("convert rebuttals to comments"))

    if not user.can_convert_to_comment(rebuttal):
        raise NotEnoughRepPointsException(_("convert rebuttals to comments"))

    if not request.POST:
        description = lambda a: _("Rebuttal by %(uname)s: %(snippet)s...") % {'uname': smart_unicode(a.author.username),
                                                                            'snippet': a.summary[:10]}
        nodes = [(argument.id, _("Argument"))]
        [nodes.append((a.id, description(a))) for a in
         argument.rebuttals.filter_state(deleted=False).exclude(id=rebuttal.id)]

        return render_to_response('node/convert_to_comment.html', {'rebuttal': rebuttal, 'nodes': nodes})

    try:
        new_parent = Node.objects.get(id=request.POST.get('under', None))
    except:
        raise CommandException(_("That is an invalid post to put the comment under"))

    if not (new_parent == argument or (new_parent.node_type == 'rebuttal' and new_parent.parent == argument)):
        raise CommandException(_("That is an invalid post to put the comment under"))

    RebuttalToCommentAction(user=user, node=rebuttal, ip=request.META['REMOTE_ADDR']).save(data=dict(new_parent=new_parent))

    return RefreshPageCommand()

@decorate.withfn(command)
def convert_comment_to_rebuttal(request, id):
    user = request.user
    comment = get_object_or_404(Comment, id=id)
    parent = comment.parent

    if not parent.argument:
        argument = parent
    else:
        argument = parent.argument
    
    if not user.is_authenticated():
        raise AnonymousNotAllowedException(_("convert comments to rebuttals"))

    if not user.can_convert_comment_to_rebuttal(comment):
        raise NotEnoughRepPointsException(_("convert comments to rebuttals"))
    
    CommentToRebuttalAction(user=user, node=comment, ip=request.META['REMOTE_ADDR']).save(data=dict(argument=argument))

    return RefreshPageCommand()

@decorate.withfn(command)
def subscribe(request, id, user=None):
    if user:
        try:
            user = User.objects.get(id=user)
        except User.DoesNotExist:
            raise Http404()

        if not (request.user.is_a_super_user_or_staff() or user.is_authenticated()):
            raise CommandException(_("You do not have the correct credentials to preform this action."))
    else:
        user = request.user

    argument = get_object_or_404(Argument, id=id)

    try:
        subscription = ArgumentSubscription.objects.get(argument=argument, user=user)
        subscription.delete()
        subscribed = False
    except:
        subscription = ArgumentSubscription(argument=argument, user=user, auto_subscription=False)
        subscription.save()
        subscribed = True

    return {
        'commands': {
            'set_subscription_button': [subscribed and _('unsubscribe me') or _('subscribe me')],
            'set_subscription_status': ['']
        }
    }

#internally grouped views - used by the tagging system
@ajax_login_required
def mark_tag(request, tag=None, **kwargs):#tagging system
    action = kwargs['action']
    ts = MarkedTag.objects.filter(user=request.user, tag__name=tag)
    if action == 'remove':
        logging.debug('deleting tag %s' % tag)
        ts.delete()
    else:
        reason = kwargs['reason']
        if len(ts) == 0:
            try:
                t = Tag.objects.get(name=tag)
                mt = MarkedTag(user=request.user, reason=reason, tag=t)
                mt.save()
            except:
                pass
        else:
            ts.update(reason=reason)
    return HttpResponse(simplejson.dumps(''), mimetype="application/json")

def matching_tags(request):
    if len(request.GET['q']) == 0:
        raise CommandException(_("Invalid request"))

    possible_tags = Tag.active.filter(name__icontains = request.GET['q'])
    tag_output = ''
    for tag in possible_tags:
        tag_output += "%s|%s|%s\n" % (tag.id, tag.name, tag.used_count)

    return HttpResponse(tag_output, mimetype="text/plain")

def matching_users(request):
    if len(request.GET['q']) == 0:
        raise CommandException(_("Invalid request"))

    possible_users = User.objects.filter(username__icontains = request.GET['q'])
    output = ''

    for user in possible_users:
        output += ("%s|%s|%s\n" % (user.id, user.decorated_name, user.reputation))

    return HttpResponse(output, mimetype="text/plain")

def related_posts(request):
    if request.POST and request.POST.get('title', None):
        can_rank, posts = RootNode.objects.search(request.POST['title'])

        if can_rank and isinstance(can_rank, basestring):
            posts = posts.order_by(can_rank)

        return HttpResponse(simplejson.dumps(
                [dict(title=p.headline, url=p.get_absolute_url(), score=p.score, summary=p.summary)
                    for p in posts.viewable_by_user(
                        request.user).filter_state(deleted=False)[0:10]]), mimetype="application/json")
    else:
        raise Http404()

def related_issues(request):
    if request.POST and request.POST.get('title', None):
        can_rank, issues = Issue.objects.search(request.POST['title'])

        if can_rank and isinstance(can_rank, basestring):
            issues = issues.order_by(can_rank)

        return HttpResponse(simplejson.dumps(
                [dict(title=q.title, url=q.get_absolute_url(), score=q.score, summary=q.summary)
                    for q in issues.viewable_by_user(
                        request.user).filter_state(deleted=False)[0:10]]), mimetype="application/json")
    else:
        raise Http404()

def related_arguments(request):
    if request.POST and request.POST.get('title', None):
        can_rank, arguments = Argument.objects.search(request.POST['title'])

        if can_rank and isinstance(can_rank, basestring):
            arguments = arguments.order_by(can_rank)

        return HttpResponse(simplejson.dumps(
                [dict(title=p.headline, url=p.get_absolute_url(), score=p.score, summary=p.summary)
                    for p in arguments.viewable_by_user(
                        request.user).filter_state(deleted=False)[0:10]]), mimetype="application/json")
    else:
        raise Http404()

@decorate.withfn(command)
def rebuttal_permanent_link(request, id):
    # Getting the current rebuttal object
    rebuttal = get_object_or_404(Rebuttal, id=id)

    # Getting the current object URL -- the Application URL + the object relative URL
    url = '%s%s' % (settings.APP_BASE_URL, rebuttal.get_absolute_url())

    if not request.POST:
        # Display the template
        return render_to_response('node/permanent_link.html', { 'url' : url, })

    return {
        'commands' : {
            'copy_url' : [request.POST['permanent_link_url'],],
        },
        'message' : _("The permanent URL to the rebuttal has been copied to your clipboard."),
    }

@decorate.withfn(command)
def award_points(request, user_id, rebuttal_id):
    user = request.user
    awarded_user = get_object_or_404(User, id=user_id)
    rebuttal = get_object_or_404(Rebuttal, id=rebuttal_id)

    # Users shouldn't be able to award themselves
    if awarded_user.id == user.id:
        raise CannotDoOnOwnException(_("award"))

    # Anonymous users cannot award  points, they just don't have such
    if not user.is_authenticated():
        raise AnonymousNotAllowedException(_('award'))

    if not request.POST:
        return render_to_response("node/award_points.html", { 'user' : user, 'awarded_user' : awarded_user, })
    else:
        points = int(request.POST['points'])

        # We should check if the user has enough reputation points, otherwise we raise an exception.
        if points < 0:
            raise CommandException(_("The number of points to award needs to be a positive value."))

        if user.reputation < points:
            raise NotEnoughRepPointsException(_("award"))

        extra = dict(message=request.POST.get('message', ''), awarding_user=request.user.id, value=points)

        # We take points from the awarding user
        AwardPointsAction(user=request.user, node=rebuttal, extra=extra).save(data=dict(value=points, affected=awarded_user))

        return { 'message' : _("You have awarded %(awarded_user)s with %(points)d points") % {'awarded_user' : awarded_user, 'points' : points} }
