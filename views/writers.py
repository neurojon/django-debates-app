# encoding:utf-8
import os.path

import datetime

from django.core.urlresolvers import reverse
from django.core.files.storage import FileSystemStorage
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.utils.html import *
from django.utils.translation import ugettext as _

from django.contrib import messages

from forum.actions import *
from forum.forms import *
from forum.models import *
from forum.utils import html
from forum.http_responses import HttpResponseUnauthorized

from vars import PENDING_SUBMISSION_SESSION_ATTR

@csrf_exempt
def upload(request):#ajax upload file to a argument or rebuttal
    class FileTypeNotAllow(Exception):
        pass
    class FileSizeNotAllow(Exception):
        pass
    class UploadPermissionNotAuthorized(Exception):
        pass

    xml_template = "<result><msg><![CDATA[%s]]></msg><error><![CDATA[%s]]></error><file_url>%s</file_url></result>"

    try:
        f = request.FILES['file-upload']
        # check upload permission
        if not request.user.can_upload_files():
            raise UploadPermissionNotAuthorized()

        # check file type
        try:
            file_name_suffix = os.path.splitext(f.name)[1].lower()
        except KeyError:
            raise FileTypeNotAllow()

        if not file_name_suffix in ('.jpg', '.jpeg', '.gif', '.png', '.bmp', '.tiff', '.ico'):
            raise FileTypeNotAllow()

        storage = FileSystemStorage(str(settings.UPFILES_FOLDER), str(settings.UPFILES_ALIAS))
        new_file_name = storage.save("_".join(f.name.split()), f)
        # check file size
        # byte
        size = storage.size(new_file_name)

        if size > float(settings.ALLOW_MAX_FILE_SIZE) * 1024 * 1024:
            storage.delete(new_file_name)
            raise FileSizeNotAllow()

        result = xml_template % ('Good', '', str(settings.UPFILES_ALIAS) + new_file_name)
    except UploadPermissionNotAuthorized:
        result = xml_template % ('', _('uploading images is limited to users with >60 reputation points'), '')
    except FileTypeNotAllow:
        result = xml_template % ('', _("allowed file types are 'jpg', 'jpeg', 'gif', 'bmp', 'png', 'tiff'"), '')
    except FileSizeNotAllow:
        result = xml_template % ('', _("maximum upload file size is %sM") % settings.ALLOW_MAX_FILE_SIZE, '')
    except Exception, e:
        result = xml_template % ('', _('Error uploading file. Please contact the site administrator. Thank you. %s' % e), '')

    return HttpResponse(result, mimetype="application/xml")

def new_argument(request, parent_id=None):
    form = None

    parent = None
    if parent_id is None:
        parent = RootNode.objects.filter_state(id=parent_id)

    if request.POST:
        if request.session.pop('reviewing_pending_data', False):
            form = NewArgumentForm(initial=request.POST, user=request.user)
        elif "text" in request.POST:
            form = NewArgumentForm(request.POST, user=request.user)
            if form.is_valid():
                if request.user.is_authenticated() and request.user.email_valid_for_creating_argument():
                    if parent is not None:
                        form.cleaned_data['parent'] = parent
                    the_action = CreateArgumentAction(user=request.user, 
                        ip=request.META['REMOTE_ADDR']).save(form.cleaned_data)
                    argument = the_action.node

                    if settings.WIKI_ON and request.POST.get('wiki', False):
                        argument.nstate.wiki = the_action

                    if request.POST.get('publish', False):
                        PublishAction(user=request.user, node=argument, ip=request.META['REMOTE_ADDR']).save()

                    return HttpResponseRedirect(argument.get_absolute_url())
                else:
                    request.session[PENDING_SUBMISSION_SESSION_ATTR] = {
                        'POST': request.POST,
                        'data_name': _("argument"),
                        'type': 'new_argument',
                        'submission_url': reverse('new_argument'),
                        'time': datetime.datetime.now()
                    }

                    if request.user.is_authenticated():
                        messages.add_message(request, messages.INFO, 
                            _("Your argument is pending until you %s.") % html.hyperlink(
                            reverse('send_validation_email'), _("validate your email")
                        ))
                        return HttpResponseRedirect(reverse('index'))
                    else:
                        return HttpResponseRedirect(reverse('auth_signin'))
        elif "go" in request.POST:
            form = NewArgumentForm({'title': request.POST['q']}, user=request.user)
            
    if not form:
        form = NewArgumentForm(user=request.user)

    return render_to_response('new_argument.html', {
        'form' : form,
        'tab' : 'argument'
        }, context_instance=RequestContext(request))

def new_issue(request):
    form = None

    if request.POST:
        if request.session.pop('reviewing_pending_data', False):
            form = NewIssueForm(initial=request.POST, user=request.user)
        elif "text" in request.POST:
            form = NewIssueForm(request.POST, user=request.user)
            if form.is_valid():
                if request.user.is_authenticated() and request.user.email_valid_for_creating_issue():
                    the_action = CreateIssueAction(user=request.user, ip=request.META['REMOTE_ADDR']).save(data=form.cleaned_data)
                    issue = the_action.node

                    if settings.WIKI_ON and request.POST.get('wiki', False):
                        issue.nstate.wiki = the_action

                    if request.POST.get('publish', False):
                        PublishAction(user=request.user, node=issue, ip=request.META['REMOTE_ADDR']).save()

                    return HttpResponseRedirect(issue.get_absolute_url())
                else:
                    request.session[PENDING_SUBMISSION_SESSION_ATTR] = {
                        'POST': request.POST,
                        'data_name': _("issue"),
                        'type': 'new_issue',
                        'submission_url': reverse('new_issue'),
                        'time': datetime.datetime.now()
                    }

                    if request.user.is_authenticated():
                        messages.add_message(request, messages.INFO, 
                            _("Your issue is pending until you %s.") % html.hyperlink(
                            reverse('send_validation_email'), _("validate your email")
                        ))
                        return HttpResponseRedirect(reverse('index'))
                    else:
                        return HttpResponseRedirect(reverse('auth_signin'))
        elif "go" in request.POST:
            form = NewIssueForm({'title': request.POST['q']}, user=request.user)
            
    if not form:
        form = NewIssueForm(user=request.user)

    return render_to_response('new_issue.html', {
        'form' : form,
        'tab' : 'issue'
        }, context_instance=RequestContext(request))

def new_position(request, id):
    form = None

    issue = get_object_or_404(Issue, id=id)

    if request.POST:
        if request.session.pop('reviewing_pending_data', False):
            form = NewPositionForm(initial=request.POST, user=request.user)
        elif "text" in request.POST:
            form = NewPositionForm(request.POST, user=request.user)
            if form.is_valid():
                if request.user.is_authenticated() and request.user.email_valid_for_creating_reason():
                    the_action = CreatePositionAction(
                        user=request.user,
                        ip=request.META['REMOTE_ADDR']).save(data=dict(
                            issue=issue, 
                            **form.cleaned_data))

                    # ??? -- if settings.WIKI_ON and request.POST.get('wiki', False):
                    #    reason.nstate.wiki = the_action

                    if request.POST.get('publish', False):
                        PublishAction(user=request.user, node=the_action.node, ip=request.META['REMOTE_ADDR']).save()

                    return HttpResponseRedirect(the_action.node.get_absolute_url())
                else:
                    request.session[PENDING_SUBMISSION_SESSION_ATTR] = {
                        'POST': request.POST,
                        'data_name': _("position"),
                        'type': 'new_position',
                        'submission_url': reverse('new_position'),
                        'time': datetime.datetime.now()
                    }

                    if request.user.is_authenticated():
                        messages.add_message(request, messages.INFO, 
                            _("Your position is pending until you %s.") % html.hyperlink(
                            reverse('send_validation_email'), _("validate your email")
                        ))
                        return HttpResponseRedirect(reverse('index'))
                    else:
                        return HttpResponseRedirect(reverse('auth_signin'))
        elif "go" in request.POST:
            form = NewPositionForm({'title': request.POST['q']}, user=request.user)
            
    if not form:
        form = NewPositionForm(user=request.user,initial={'tags':issue.tagnames})

    return render_to_response('new_position.html', {
        'issue' : issue,
        'form' : form,
        'tab' : 'position'
        }, context_instance=RequestContext(request))

def new_reason_for(request, id):
    return new_reason(request, id, False)

def new_reason_against(request, id):
    return new_reason(request, id, True)

def new_reason(request, id, opposing):
    form = None

    position = get_object_or_404(Position, id=id)

    if request.POST:
        if request.session.pop('reviewing_pending_data', False):
            form = NewReasonForm(initial=request.POST, user=request.user)
        elif "text" in request.POST:
            form = NewReasonForm(request.POST, user=request.user)
            if form.is_valid():
                if request.user.is_authenticated() and request.user.email_valid_for_creating_reason():
                    the_action = CreateReasonAction(
                        opposes_parent=opposing,
                        user=request.user,
                        ip=request.META['REMOTE_ADDR']).save(data=dict(
                            position=position, 
                            opposes_parent=opposing,
                            **form.cleaned_data))

                    reason = the_action.node

                    if settings.WIKI_ON and request.POST.get('wiki', False):
                        reason.nstate.wiki = the_action

                    if request.POST.get('publish', False):
                        PublishAction(user=request.user, node=reason, ip=request.META['REMOTE_ADDR']).save()

                    if position.parent is not None:
                        redirect_node = position.parent
                    else:
                        redirect_node = position
                    return HttpResponseRedirect(redirect_node.get_absolute_url())
                else:
                    request.session[PENDING_SUBMISSION_SESSION_ATTR] = {
                        'POST': request.POST,
                        'data_name': _("reason"),
                        'type': 'new_reason',
                        'submission_url': reverse('new_reason'),
                        'time': datetime.datetime.now()
                    }

                    if request.user.is_authenticated():
                        messages.add_message(request, messages.INFO, 
                            _("Your reason is pending until you %s.") % html.hyperlink(
                            reverse('send_validation_email'), _("validate your email")
                        ))
                        return HttpResponseRedirect(reverse('index'))
                    else:
                        return HttpResponseRedirect(reverse('auth_signin'))
        elif "go" in request.POST:
            form = NewReasonForm({'title': request.POST['q']}, user=request.user)
            
    if not form:
        form = NewReasonForm(user=request.user)

    return render_to_response('new_reason.html', {
        'position' : position,
        'form' : form,
        'tab' : 'reason',
        'opposes_parent' : opposing,
        }, context_instance=RequestContext(request))


#def convert_to_argument(request, id):
#    user = request.user
#
#    node_type = request.GET.get('node_type', 'rebuttal')
#    if node_type == 'comment':
#        node = get_object_or_404(Comment, id=id)
#        action_class = CommentToArgumentAction
#    else:
#        node = get_object_or_404(Answer, id=id)
#        action_class = AnswerToArgumentAction
#
#    if not user.can_convert_to_argument(node):
#        return HttpResponseUnauthorized(request)
#
#    return _edit_argument(request, node, template='node/convert_to_argument.html', summary=_("Converted to argument"),
#                           action_class =action_class, allow_rollback=False, url_getter=lambda a: Argument.objects.get(id=a.id).get_absolute_url())

def edit_argument(request, id):
    argument = get_object_or_404(Argument, id=id)
    if argument.nis.deleted and not request.user.can_view_deleted_post(argument):
        raise Http404
    if request.user.can_edit_post(argument):
        return _edit_argument(request, argument)
    elif request.user.can_retag_arguments():
        return _retag_argument(request, argument)
    else:
        raise Http404

def edit_post(request, id):
    post = get_object_or_404(Node, id=id)
    post_type = post.node_type

    if post.nis.deleted and not request.user.can_view_deleted_post(post):
        raise Http404
    if request.user.can_edit_post(post):
        if post_type == 'argument':
            return _edit_argument(request, post)
        elif post_type == 'rebuttal':
            return _edit_rebuttal(request, post)
        elif post_type == 'issue':
            return _edit_issue(request, post)
        elif post_type == 'position':
            return _edit_position(request, post)
        elif post_type == 'reason':
            return _edit_reason(request, post)
        else:
            raise Http404
    elif post_type=='argument' and request.user.can_retag_arguments():
        return _retag_argument(request, argument)
    else:
        raise Http404
    

def _retag_argument(request, argument):
    if request.method == 'POST':
        form = RetagArgumentForm(argument, request.POST)
        if form.is_valid():
            if form.has_changed():
                RetagAction(user=request.user, node=argument, ip=request.META['REMOTE_ADDR']).save(data=dict(tagnames=form.cleaned_data['tags']))

            return HttpResponseRedirect(argument.get_absolute_url())
    else:
        form = RetagArgumentForm(argument)
    return render_to_response('argument_retag.html', {
        'argument': argument,
        'form' : form,
        #'tags' : _get_tags_cache_json(),
    }, context_instance=RequestContext(request))

def _edit_argument(request, argument, template='argument_edit.html', summary='', action_class=ReviseAction, allow_rollback=True, url_getter=lambda q: q.get_absolute_url()):
    if request.method == 'POST':
        revision_form = RevisionForm(argument, data=request.POST)
        revision_form.is_valid()
        revision = argument.revisions.get(revision=revision_form.cleaned_data['revision'])

        if 'select_revision' in request.POST:
            form = EditArgumentForm(argument, request.user, revision)
        else:
            form = EditArgumentForm(argument, request.user, revision, data=request.POST)

        if not 'select_revision' in request.POST and form.is_valid():
            if form.has_changed():
                action = action_class(user=request.user, node=argument, ip=request.META['REMOTE_ADDR']).save(data=form.cleaned_data)

                if settings.WIKI_ON:
                    if request.POST.get('wiki', False) and not argument.nis.wiki:
                        argument.nstate.wiki = action
                    elif argument.nis.wiki and (not request.POST.get('wiki', False)) and request.user.can_cancel_wiki(argument):
                        argument.nstate.wiki = None
            else:
                if not revision == argument.active_revision:
                    if allow_rollback:
                        RollbackAction(user=request.user, node=argument).save(data=dict(activate=revision))
                    else:
                        pass

            return HttpResponseRedirect(url_getter(argument))
    else:
        revision_form = RevisionForm(argument)
        form = EditArgumentForm(argument, request.user, initial={'summary': summary})

    return render_to_response(template, {
        'argument': argument,
        'revision_form': revision_form,
        'form' : form,
    }, context_instance=RequestContext(request))

def _edit_issue(request, issue, template='issue_edit.html', summary='', action_class=ReviseAction, allow_rollback=True, url_getter=lambda q: q.get_absolute_url()):
    if request.method == 'POST':
        revision_form = RevisionForm(issue, data=request.POST)
        revision_form.is_valid()
        revision = issue.revisions.get(revision=revision_form.cleaned_data['revision'])

        if 'select_revision' in request.POST:
            form = EditIssueForm(issue, request.user, revision)
        else:
            form = EditIssueForm(issue, request.user, revision, data=request.POST)

        if not 'select_revision' in request.POST and form.is_valid():
            if form.has_changed():
                action = action_class(user=request.user, node=issue, \
                    ip=request.META['REMOTE_ADDR']).save(data=form.cleaned_data)

                if settings.WIKI_ON:
                    if request.POST.get('wiki', False) and not issue.nis.wiki:
                        issue.nstate.wiki = action
                    elif issue.nis.wiki and (not request.POST.get('wiki', False)) and \
                            request.user.can_cancel_wiki(issue):
                        rebuttal.nstate.wiki = None

            else:
                if not revision == issue.active_revision:
                    if allow_rollback:
                        RollbackAction(user=request.user, node=issue).save(data=dict(activate=revision))
                    else:
                        pass

            return HttpResponseRedirect(url_getter(issue))
    else:
        revision_form = RevisionForm(issue)
        form = EditIssueForm(issue, request.user, initial={'summary': summary})

    return render_to_response(template, {
        'issue': issue,
        'revision_form': revision_form,
        'form' : form,
    }, context_instance=RequestContext(request))

def _edit_position(request, position, template='position_edit.html', summary='', action_class=ReviseAction, allow_rollback=True, url_getter=lambda q: q.get_absolute_url()):
    if request.method == 'POST':
        revision_form = RevisionForm(position, data=request.POST)
        revision_form.is_valid()
        revision = position.revisions.get(revision=revision_form.cleaned_data['revision'])

        if 'select_revision' in request.POST:
            form = EditPositionForm(position, request.user, revision)
        else:
            form = EditPositionForm(position, request.user, revision, data=request.POST)

        if not 'select_revision' in request.POST and form.is_valid():
            if form.has_changed():
                action = action_class(user=request.user, node=position, \
                    ip=request.META['REMOTE_ADDR']).save(data=form.cleaned_data)

                if settings.WIKI_ON:
                    if request.POST.get('wiki', False) and not position.nis.wiki:
                        position.nstate.wiki = action
                    elif position.nis.wiki and (not request.POST.get('wiki', False)) and \
                            request.user.can_cancel_wiki(position):
                        rebuttal.nstate.wiki = None

            else:
                if not revision == position.active_revision:
                    if allow_rollback:
                        RollbackAction(user=request.user, node=position).save(data=dict(activate=revision))
                    else:
                        pass

            return HttpResponseRedirect(url_getter(position))
    else:
        revision_form = RevisionForm(position)
        form = EditPositionForm(position, request.user, initial={'summary': summary})

    return render_to_response(template, {
        'position': position,
        'revision_form': revision_form,
        'form' : form,
    }, context_instance=RequestContext(request))


def publish_argument(request, id):
    return publish_post(request,id)

def publish_issue(request, id):
    return publish_post(request,id)

def publish_post(request, id):
    post = get_object_or_404(RootNode, id=id)
    if not post.nis.published and request.user==post.author:
        return _publish_post(request, post)
    else:
        raise Http404

def _publish_post(request, post ):
    action = PublishAction(user=request.user, node=post, ip=request.META['REMOTE_ADDR']).save()
    return HttpResponseRedirect(post.get_absolute_url())
    
def promote_argument(request, id):
    argument = get_object_or_404(Argument, id=id)
    if not argument.nis.promoted and request.user.can_promote_argument(argument):
        return _promote_argument(request, argument)
    else:
        raise Http404

def _promote_argument(request, argument):
    action = PromoteAction(user=request.user, node=argument, ip=request.META['REMOTE_ADDR']).save()
    return HttpResponseRedirect(argument.get_absolute_url())
    
def _edit_rebuttal(request, rebuttal):
    if request.method == "POST":
        revision_form = RevisionForm(rebuttal, data=request.POST)
        revision_form.is_valid()
        revision = rebuttal.revisions.get(revision=revision_form.cleaned_data['revision'])

        if 'select_revision' in request.POST:
            form = EditRebuttalForm(rebuttal, request.user, revision)
        else:
            form = EditRebuttalForm(rebuttal, request.user, revision, data=request.POST)

        if not 'select_revision' in request.POST and form.is_valid():
            if form.has_changed():
                action = ReviseAction(user=request.user, node=rebuttal, ip=request.META['REMOTE_ADDR']).save(data=form.cleaned_data)

                if settings.WIKI_ON:
                    if request.POST.get('wiki', False) and not rebuttal.nis.wiki:
                        rebuttal.nstate.wiki = action
                    elif rebuttal.nis.wiki and (not request.POST.get('wiki', False)) and request.user.can_cancel_wiki(rebuttal):
                        rebuttal.nstate.wiki = None
            else:
                if not revision == rebuttal.active_revision:
                    RollbackAction(user=request.user, node=rebuttal, ip=request.META['REMOTE_ADDR']).save(data=dict(activate=revision))

            return HttpResponseRedirect(rebuttal.get_absolute_url())

    else:
        revision_form = RevisionForm(rebuttal)
        form = EditRebuttalForm(rebuttal, request.user)
    return render_to_response('rebuttal_edit.html', {
                              'rebuttal': rebuttal,
                              'revision_form': revision_form,
                              'form': form,
                              }, context_instance=RequestContext(request))

def _edit_reason(request, reason):
    if request.method == "POST":
        revision_form = RevisionForm(reason, data=request.POST)
        revision_form.is_valid()
        revision = reason.revisions.get(revision=revision_form.cleaned_data['revision'])

        if 'select_revision' in request.POST:
            form = EditReasonForm(reason, request.user, revision)
        else:
            form = EditReasonForm(reason, request.user, revision, data=request.POST)

        if not 'select_revision' in request.POST and form.is_valid():
            if form.has_changed():
                action = ReviseAction(user=request.user, node=reason, ip=request.META['REMOTE_ADDR']).save(data=form.cleaned_data)
                if settings.WIKI_ON:
                    if request.POST.get('wiki', False) and not reason.nis.wiki:
                        reason.nstate.wiki = action
                    elif reason.nis.wiki and (not request.POST.get('wiki', False)) and \
                            request.user.can_cancel_wiki(reason):
                        rebuttal.nstate.wiki = None


            return HttpResponseRedirect(reason.get_absolute_url())

    else:
        revision_form = RevisionForm(reason)
        form = EditReasonForm(reason, request.user)
    return render_to_response('reason_edit.html', {
                              'position' : reason.parent,
                              'reason': reason,
                              'revision_form': revision_form,
                              'form': form,
                              }, context_instance=RequestContext(request))

def new_rebuttal(request, id):
    argument = get_object_or_404(Argument, id=id)

    if request.POST:
        form = NewRebuttalForm(request.POST, request.user)

        if request.session.pop('reviewing_pending_data', False) or not form.is_valid():
            request.session['redirect_POST_data'] = request.POST
            return HttpResponseRedirect(argument.get_absolute_url() + '#fmrebuttal')

        if request.user.is_authenticated() and request.user.email_valid_for_creating_rebuttal():
            rebuttal_action = CreateRebuttalAction(user=request.user, ip=request.META['REMOTE_ADDR']).save(dict(argument=argument, **form.cleaned_data))
            rebuttal = rebuttal_action.node

            if settings.WIKI_ON and request.POST.get('wiki', False):
                rebuttal.nstate.wiki = rebuttal_action

            return HttpResponseRedirect(rebuttal.get_absolute_url())
        else:
            request.session[PENDING_SUBMISSION_SESSION_ATTR] = {
                'POST': request.POST,
                'data_name': _("rebuttal"),
                'type': 'rebuttal',
                'submission_url': reverse('rebuttal', kwargs={'id': id}),
                'time': datetime.datetime.now()
            }

            if request.user.is_authenticated():
                messages.add_message(request, messages.INFO, 
                            _("Your rebuttal is pending until you %s.") % html.hyperlink(
                            reverse('send_validation_email'), _("validate your email")
                        ))
                return HttpResponseRedirect(argument.get_absolute_url())
            else:
                return HttpResponseRedirect(reverse('auth_signin'))

    return HttpResponseRedirect(argument.get_absolute_url())


def manage_pending_data(request, action, forward=None):
    pending_data = request.session.pop(PENDING_SUBMISSION_SESSION_ATTR, None)

    if not pending_data:
        raise Http404

    if action == _("cancel"):
        return HttpResponseRedirect(forward or request.META.get('HTTP_REFERER', '/'))
    else:
        if action == _("review"):
            request.session['reviewing_pending_data'] = True

        request.session['redirect_POST_data'] = pending_data['POST']
        return HttpResponseRedirect(pending_data['submission_url'])


